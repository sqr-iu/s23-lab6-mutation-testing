from typing import Tuple, List

import pytest

from bonus_system import calculateBonuses

# params: List[Tuple[level, amount, result]]
params: List[Tuple[str, int, float]] = [
    ("Standard", 5000, 0.5),
    ("Standard", 10000, 0.5 * 1.5),
    ("Standard", 25000, 0.5 * 1.5),
    ("Standard", 50000, 0.5 * 2),
    ("Standard", 75000, 0.5 * 2),
    ("Standard", 100000, 0.5 * 2.5),
    ("Standard", 250000, 0.5 * 2.5),
    ("Premium", 5000, 0.1),
    ("Premium", 10000, 0.1 * 1.5),
    ("Premium", 25000, 0.1 * 1.5),
    ("Premium", 50000, 0.1 * 2),
    ("Premium", 75000, 0.1 * 2),
    ("Premium", 100000, 0.1 * 2.5),
    ("Premium", 250000, 0.1 * 2.5),
    ("Diamond", 5000, 0.2),
    ("Diamond", 10000, 0.2 * 1.5),
    ("Diamond", 25000, 0.2 * 1.5),
    ("Diamond", 50000, 0.2 * 2),
    ("Diamond", 75000, 0.2 * 2),
    ("Diamond", 100000, 0.2 * 2.5),
    ("Diamond", 250000, 0.2 * 2.5),
    ("", 5000, 0),
    ("", 10000, 0),
    ("", 25000, 0),
    ("", 50000, 0),
    ("", 75000, 0),
    ("", 100000, 0),
    ("", 250000, 0),
    ("abc", 5000, 0),
    ("xyz", 5000, 0),
]


@pytest.mark.parametrize("s, b, r", params)
def test_calculateBonuses(s, b, r):
    assert calculateBonuses(s, b) == r
